package com.wuze.provider;


import com.wuze.TCC.Processor.Processor;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * @author 19414
 */
@SpringBootApplication
@EnableDubbo
@MapperScan(basePackages = {"com.wuze.provider.mapper"})
@Import(Processor.class)
public class ProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class,args);
    }
}
