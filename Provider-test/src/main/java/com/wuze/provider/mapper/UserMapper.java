package com.wuze.provider.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuze.api.pojo.User;

/**
 * @author 19414
 */
public interface UserMapper extends BaseMapper<User> {
}
