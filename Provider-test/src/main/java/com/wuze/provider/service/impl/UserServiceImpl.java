package com.wuze.provider.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuze.api.pojo.User;
import com.wuze.provider.mapper.UserMapper;
import com.wuze.provider.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author 19414
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
