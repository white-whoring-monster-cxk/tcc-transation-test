package com.wuze.provider.rpcservice;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuze.TCC.annotation.TCC;
import com.wuze.api.pojo.User;

import com.wuze.api.rpcservice.TestService;
import com.wuze.provider.service.UserService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 19414
 */

@Service(version = "1.0.0")
@Component
public class TestServiceImpl implements TestService {

    @Autowired
    UserService userService;

    @Override
    @TCC(confirm = "confirm",cancel = "cancel",isInitiator = false)
    public User test(String name,int money) throws InterruptedException {
        Thread.sleep(5000);
        System.out.println("添加金额:"+money);
        User one = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getUsername, name));
        one.setMoney(one.getMoney()+money);
        userService.update(one, new QueryWrapper<User>().lambda().eq(User::getUsername, name));
        //手动异常
//        int a = 1/0;
        return one;
    }
    public void confirm(String name,int money){
        System.out.println("添加成功");
    }
    public void cancel(String name,int money){
        System.out.println("扣减金额:"+money);
        User one = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getUsername, name));
        one.setMoney(one.getMoney()-money);
        userService.update(one,new QueryWrapper<User>().lambda().eq(User::getUsername,name));
    }

}
