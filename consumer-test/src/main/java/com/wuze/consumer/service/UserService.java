package com.wuze.consumer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wuze.api.pojo.User;


/**
 * @author 19414
 */
public interface UserService extends IService<User> {
}
