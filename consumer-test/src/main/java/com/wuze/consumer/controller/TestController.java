package com.wuze.consumer.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuze.TCC.annotation.TCC;
import com.wuze.api.pojo.User;

import com.wuze.api.rpcservice.TestService;
import com.wuze.consumer.service.UserService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Reference(version = "1.0.0",retries = 0,timeout = 10000)
    TestService testService;

    @Autowired
    UserService userService;

    @GetMapping("/test")
    @TCC(confirm = "confirm", cancel = "cancel", isInitiator = true)
    public User test(String username, String name, int money) throws InterruptedException {
        //手动异常
        int a = 1/0;
        System.out.println("扣减金额:" + money);
        User one = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getUsername, username));
        one.setMoney(one.getMoney() - money);
        userService.update(one, new QueryWrapper<User>().lambda().eq(User::getUsername, username));
        User test = testService.test(name, money);
        return test;
    }

    public void confirm(String username, String name, int money) {
        System.out.println("转账成功");
    }

    public void cancel(String username, String name, int money) {
        System.out.println("添加金额:"+money);
        User one = userService.getOne(new QueryWrapper<User>().lambda().eq(User::getUsername, username));
        one.setMoney(one.getMoney() + money);
        userService.update(one, new QueryWrapper<User>().lambda().eq(User::getUsername, username));
    }

}
