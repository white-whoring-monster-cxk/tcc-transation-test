package com.wuze.consumer;


import com.wuze.TCC.Processor.Processor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * @author 19414
 */
@SpringBootApplication
@MapperScan("com.wuze.consumer.mapper")
@Import(Processor.class)
public class ConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class,args);
    }
}
